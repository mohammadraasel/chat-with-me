import React from 'react'
import './app.scss'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom"
 
import Authentication from '../views/pages/authentication';
import Home from '../views/pages/home';
import NotFound from '../views/pages/not-found';
import './app.scss';
 
class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Authentication} />
          <Route path="/home/" component={Home} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    )
  }
}
 
export default App
