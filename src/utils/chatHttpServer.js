import axios from 'axios';
 
class ChatHttpServer {

    getUserId() {
        return new Promise((resolve, reject) => {
            try {
                resolve(localStorage.getItem('userid'));
            } catch (error) {
                reject(error);
            }
        });
    }

    removeLS() {
        return new Promise((resolve, reject) => {
            try {
                localStorage.removeItem('userid');
                localStorage.removeItem('username');
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    setLS(key, value) {
        return new Promise((resolve, reject) => {
            try {
                localStorage.setItem(key, value);
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    login(userCredential) {
        const config = {
            headers: {
                "Content-type": "application/json"
            }
        }
        return new Promise(async (resolve, reject) => {
            try {
                const response = await axios.post('http://localhost:4000/login', userCredential, config);
                resolve(response.data);
            } catch (error) {
                reject(error);
            }
        });
    }

    checkUsernameAvailability(username) {
        const config = {
            headers: {
                "Content-type": "application/json"
            }
        }
        return new Promise(async (resolve, reject) => {
            try {
                const response = await axios.post('http://localhost:4000/usernameAvailable', {
                    username: username
                }, config);
                resolve(response.data);
            } catch (error) {
                reject(error);
            }
        });
    }
    
    register(userCredential) {
        const config = {
            headers: {
                "Content-type": "application/json"
            }
        }
        return new Promise(async (resolve, reject) => {
            try {
                const response = await axios.post('http://localhost:4000/register', userCredential, config);
                resolve(response.data);
            } catch (error) {
                reject(error);
            }
        });
    }

    userSessionCheck(userId) {
        const config = {
            headers: {
                "Content-type": "application/json"
            }
        }
        return new Promise(async (resolve, reject) => {
            try {
                const response = await axios.post('http://localhost:4000/userSessionCheck', {
                    userId: userId
                },config );
                resolve(response.data);
            } catch (error) {
                reject(error);
            }
        });
    }

    getMessages(userId, toUserId) {
        const config = {
            headers: {
                "Content-type": "application/json"
            }
        }
        return new Promise(async (resolve, reject) => {
            try {
                const response = await axios.post('http://localhost:4000/getMessages', {
                    userId: userId,
                    toUserId: toUserId
                }, config);
                resolve(response.data);
            } catch (error) {
                reject(error);
            }
        });
    }
    
}

export default new ChatHttpServer();