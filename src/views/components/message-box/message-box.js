import React, { Component } from 'react'
import './message-box.scss'
import ChatHttpServer from '../../../utils/chatHttpServer'
import ChatSocketServer from '../../../utils/chatSocketServer'
import { ReactSVG } from 'react-svg'


export class MessageBox extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messageLoading: true,
			messages: [],
			selectedUser: null
		}
		this.messageContainer = React.createRef();
	}

	componentDidMount() {
		ChatSocketServer.receiveMessage();
		ChatSocketServer.eventEmitter.on('add-message-response', this.receiveSocketMessages);
		this.getMessages()
	}

	componentWillUnmount() {
		ChatSocketServer.eventEmitter.removeListener('add-message-response', this.receiveSocketMessages);
	}

	componentDidUpdate(prevProps) {
		if (prevProps.newSelectedUser === null || (this.props.newSelectedUser.id !== prevProps.newSelectedUser.id)) {
		   this.getMessages();
		}
	}

	static getDerivedStateFromProps(props, state) {
		if (state.selectedUser === null || state.selectedUser.id !== props.newSelectedUser.id) {
			return {
				selectedUser: props.newSelectedUser
			}
		}
		return null   
	}

	receiveSocketMessages = (socketResponse) => {
		const { selectedUser } = this.state;
		if (selectedUser !== null && selectedUser.id === socketResponse.fromUserId) {
			this.setState({
				messages: [...this.state.messages, socketResponse]
			})
			this.scrollMessageContainer()
		}
	}

	getMessages = async () => {
		try {
			const { userId, newSelectedUser} = this.props
			const messageResponse = await ChatHttpServer.getMessages(userId, newSelectedUser.id)
			
			if (!messageResponse.error) {
				this.setState({
					messages: messageResponse.messages,
				});
				this.scrollMessageContainer()
			} else {
				console.log('Unable to fetch messages')
			}
			this.setState({
				messageLoading: false
			});
		} catch (error) {
			this.setState({
				messageLoading: false
			});
		}
	}
	
	sendMessage = (event) => {
		if (event.key === 'Enter') {
			const message = event.target.value
			const { userId, newSelectedUser } = this.props
			if (message === '' || message === undefined || message === null) {
				console.log(`Message can't be empty.`)
			} else if (userId === '') {
				this.props.history.push('/')
			} else if (newSelectedUser === undefined) {
				console.log(`Select a user to chat.`)
			} else {
				this.sendAndUpdateMessages({
					fromUserId: userId,
					message: (message).trim(),
					toUserId: newSelectedUser.id,
				})
				event.target.value = ''
			}
		}
	}
	
	sendAndUpdateMessages(message) {
		try {
			ChatSocketServer.sendMessage(message)
			this.setState({
				messages : [...this.state.messages, message]
			});
				this.scrollMessageContainer()
		} catch (error) {
			console.log(`Can't send your message`)
		}
	}

	scrollMessageContainer() {
		if (this.messageContainer.current !== null) {
			try {
				setTimeout(() => {
					this.messageContainer.current.scrollTop = this.messageContainer.current.scrollHeight;
				}, 100);
			}catch (error) {
				console.warn(error);
			}
		}
	}

	alignMessages(toUserId) {
		const { userId } = this.props;
		return userId !== toUserId;
	}

	getMessageUI () {
		return (
			<ul className='messages-container'>
				{
					this.state.messages.map((message, index) => (
						<li
							className={`message ${this.alignMessages(message.toUserId) ? 'current-user-message' : ''}`}
							key={index}
						>	
							{
								!this.alignMessages(message.toUserId) &&
								<img
									style={{
										borderRadius: '50%',
										height: '30px',
										width: '30px',
										padding: '2px',
										backgroundColor: '#a29bfe'
									}}
									src={'male-user.png'}
									alt="user-avatar"
								/>
							}
							<span>{message.message}</span>
						</li>
					))
				}
			</ul>
		)
	}


	getInitiateConversationUI() {
		if (this.props.newSelectedUser !== null) {
			return (
			<div className="start-chatting-banner">
				<p className="heading">
					You haven 't chatted with {this.props.newSelectedUser.username} in a while,
					<span className="sub-heading"> Say Hi.</span>
				</p>			
			</div>
			)
		}    
	}

	render() {
		const { selectedUser } = this.state;
		return (	
			<div className="message-box">
				<header className='message-box__header'>
					<div className="left">
						{
							selectedUser?
								<div className="selected-user-container">
									<img style={{backgroundColor: '#a29bfe'}} src={'male-user.png'} alt="useravater" />
										<div className="info">
											<h3 className="title">{this.props.newSelectedUser.username}</h3>
											<p className="status">
												
												<span className={`dot${this.state.selectedUser.online === 'Y'? ' online': ' offline'}`}></span>
												{this.state.selectedUser.online === 'Y'? "online": "offline"}
											</p>
										</div>
								</div>
								:
								<h2>Choose a user from the chatlist</h2>
						}
					</div>
					<div className="right">
						<ReactSVG className="more-icon" src={'more-24.svg'}/>
					</div>
				</header>
				<div className="content" ref={this.messageContainer}>
					<div className="messages">
						{this.state.messages.length > 0 ? this.getMessageUI() : this.getInitiateConversationUI()}	
					</div>
				</div>
				<div className="footer">
					<div className="input-container">
						<ReactSVG className="attachment" src={'attachment.svg'}/>
						<input onKeyPress={this.sendMessage} type="text" placeholder='Write your message here...' />
					</div>
					<ReactSVG onClick={this.sendMessage} className="send-button" src={'sent-48.svg'}/>
				</div>

			</div>
		)
	}
}

export default MessageBox
