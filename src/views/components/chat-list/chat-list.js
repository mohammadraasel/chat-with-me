import React, { Component } from 'react'
import ChatSocketServer from '../../../utils/chatSocketServer'
import './chat-list.scss'

const colors = [
	'#fd79a8',
	'#55efc4',
	'#ff7675',
	'#fdcb6e',
	'#a29bfe'
]
export class ChatList extends Component {

	constructor(props) {
		super(props)
		this.state = {
			loading: true,
			selectedUserId: null,
			chatListUsers: []
		}
	}


	createChatListUsers = (chatListResponse) => {    
		if (!chatListResponse.error) {
		  	let chatListUsers = this.state.chatListUsers
		  	if (chatListResponse.singleUser) {
				if (chatListUsers.length > 0){
			  		chatListUsers = chatListUsers.filter(function (obj) {
						return obj.id !== chatListResponse.chatList[0].id;
			  		})
				}
				/* Adding new online user into chat list array */
					chatListUsers = [...chatListResponse.chatList, ...chatListUsers];
					if (this.state.selectedUserId === chatListResponse.chatList[0].id) {		
						this.props.updateSelectedUser(chatListResponse.chatList[0])
					}
		  	} else if (chatListResponse.userDisconnected) {
				const loggedOutUser = chatListUsers.findIndex((obj) => obj.id === chatListResponse.userid);
				if (loggedOutUser >= 0) {
					chatListUsers[loggedOutUser].online = 'N';
					if (this.state.selectedUserId === chatListUsers[loggedOutUser].id) {		
						this.props.updateSelectedUser(chatListUsers[loggedOutUser])
					}
				}
		  	} else {
				/* Updating entire chat list if user logs in. */
				chatListUsers = chatListResponse.chatList;
			}		
		  	this.setState({ chatListUsers: chatListUsers })			
		} else {
			console.log('unable to load chat list.')
		}
		this.setState({ loading: false })
	}

	onSelectedUser = (user) => {
		this.setState({ selectedUserId: user.id })
		this.props.updateSelectedUser(user)
	}

	componentDidMount() {
		const userId = this.props.userId;
		ChatSocketServer.getChatList(userId);
		ChatSocketServer.eventEmitter.on('chat-list-response', this.createChatListUsers);
	}

	componentWillUnmount() {
		ChatSocketServer.eventEmitter.removeListener('chat-list-response', this.createChatListUsers);
	}

	render() {
		return (
			<div className="chat-list">
				<div className='chat-list__container'>
					{
						this.state.chatListUsers.map((user, index) => {
							return (
								<div className='chat' key={index} onClick={()=> this.onSelectedUser(user)}>
									<div className="image-container">
										<img src={index%2 === 0? 'male-user.png': 'female-user.png'} style={{backgroundColor: colors[index%(colors.length)]}} alt="user" />
										<span className={ user.online === 'Y'? 'online': 'offline'}></span>
									</div>
									<div className="info">
										<h2 className='title'>{user.username}</h2>
										<p className="text">some text...</p>
									</div>
								</div>
							)
						})	
					}
					{
						this.state.loading && this.state.chatListUsers.length === 0 ?
							<div className="loading-spinner">Loading...</div> : null
					}
					{
						!this.state.loading && this.state.chatListUsers.length === 0 ?
							<div className="empty-message">No user available.</div>:null
					}
				</div>
			</div>
		)
	}
}

export default ChatList
