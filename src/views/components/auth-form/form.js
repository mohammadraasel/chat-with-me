import React, { Component } from 'react'
import './form.scss'
import ChatHttpServer from '../../../utils/chatHttpServer'
import { DebounceInput } from 'react-debounce-input'
import {withRouter} from 'react-router-dom'

export class Form extends Component {
	state = {
		username: '', 
		password: '',
		errorMessage: '',
		isLoading: false
	}

	resetState = () => {
		this.setState({
			username: '', 
			password: '',
			errorMessage: '',
			isLoading: false
		})
	}

	componentDidUpdate(prevProps) {
		if (prevProps.isLogin !== this.props.isLogin) {
			this.resetState()
		}
	}

	changeHandler = e => {
		this.setState({
			errorMessage: '',
			[e.target.id]: e.target.value
		})
	}

	redirectTo = (pathname) => {
		this.setState({
			username: '',
			password: '',
			errorMessage: '',
			isLoading: false
		}, () => {
			this.props.history.push(`/${pathname}`)	
		})
	}

	login = async(e) => {
		if (this.state.username.trim().length === 0) {
			this.setState({ errorMessage: 'Username must not be empty' })
			return 
		}

		if (this.state.password.length === 0) {
			this.setState({ errorMessage: 'Password must not be empty' })
			return 
		}

		if (this.state.errorMessage.length !== 0) return

		this.setState({ isLoading: true })
		try {
			const response = await ChatHttpServer.login(this.state);
			this.setState({ isLoading: false })

			if(response.error) {
				this.setState({ errorMessage: "Invalid Credentials." })
			} else {
				ChatHttpServer.setLS('userid', response.userId);
				this.redirectTo('home')
			}
		} catch (error) {
			this.setState({ errorMessage: "Something went wrong.", isLoading: false })
		}
	}

	signup = async (e) => {
		if (this.state.username.trim().length === 0) {
			this.setState({ errorMessage: 'Username must not be empty' })
			return 
		}

		if (this.state.password.length === 0) {
			this.setState({ errorMessage: 'Password must not be empty' })
			return 
		}

		if (this.state.errorMessage.length !== 0) return

		this.setState({ isLoading: true })
		
		const userData = {
			username: this.state.username,
			password: this.state.password
		}
		try {
			const response = await ChatHttpServer.register(userData);
			this.setState({ isLoading: false })
			
			if (response.error) {
				this.setState({errorMessage: "Unable to register, try after some time."})
			} else {
				ChatHttpServer.setLS('userid', response.userId);
				this.redirectTo('home')
			}
		} catch (error) {
		  	this.setState({errorMessage: "Unable to register, try after some time.", isLoading: false })
		}
	}

	checkUsernameAvailability = async (event)  => {
		if(event.target.value !== '' && event.target.value !== undefined) {
			this.setState({
				username: event.target.value,
				isLoading: true
			});
			
			try {
				const response = await ChatHttpServer.checkUsernameAvailability(this.state.username);
				this.setState({ isLoading: false })
				if(response.error) {
					this.setState({
						errorMessage: response.message
					})
				} else {
					this.setState({
						errorMessage: ''
					})
				}
			} catch (error) {
				this.setState({
					isLoading: false,
					errorMessage: 'something went wrong.',
				});
			}
		} 
	}


	render() {
		return (
			<div className="form">
				<div className="title-container">
					<h3>{this.props.isLogin ? "Welcome back" : "Easy Sign Up"}</h3>
					{
						this.props.isLogin ?
							<span>Credentials are to login.</span>
							:
							<span>Borring stuff.</span>
					}
				</div>
				<div className="form-group">
					{/* <label htmlFor="username">Usename</label> */}
					<DebounceInput
						className="form-control"
						placeholder = "Username"
						minLength={2}
						debounceTimeout={300}
						onChange={(e) => {
							if (this.props.isLogin) {
								this.setState({
									errorMessage: '',
									username: e.target.value
								})
							} else {
								this.checkUsernameAvailability(e)
							}
						}}
					/>
				</div>
				<div className="form-group">
					{/* <label htmlFor="password">Password</label> */}
					<input
						type="password"
						id="password"
					    className="form-control"	
						placeholder='Password'
						value={this.state.password}
						onChange={this.changeHandler}
					/>
				</div>
				<div className="button-container">
					<button
						type='submit'
						onClick={this.props.isLogin? this.login: this.signup}
					>
						{this.props.isLogin? "Login": "Register"}
					</button>
				</div>
				{
					this.state.errorMessage && <div className='error-message'>
						<p className="error">{this.state.errorMessage}</p>
					</div>
				}
			</div>
		)
	}
}

export default withRouter(Form) 
