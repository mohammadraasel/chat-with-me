import React from 'react'
import './not-found.scss'

function NotFound() {
	return (
		<div>
			Not Found.
		</div>
	)
}

export default NotFound
