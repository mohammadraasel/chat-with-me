import React, { Component } from 'react'
import './authentication.scss'
import AuthForm from '../../components/auth-form'


const tabs = [
	"LOGIN",
	"SIGNUP"
]
export class Authentication extends Component {
	state = {
		tabIndex : 0
	}
	render() {
		return (
			<div className='authentication-page'>
				<div className="tabs-container">
					{
						tabs.map((tabname, index) => {
							return (
								<div
									key={tabname}
									onClick={() =>this.setState({ tabIndex: index })}
									className={`tab${this.state.tabIndex === index ? ' selected' : ' disabled'}`}
								>
									{ tabname }
								</div>
							)
						})
					}
				</div>
				<div className="container">
					<div className="form-container">
						<AuthForm isLogin={this.state.tabIndex === 0}/>
					</div>
				</div>
			</div>
		)
	}
}

export default Authentication
