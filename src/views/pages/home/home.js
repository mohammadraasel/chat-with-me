import React, { useState, useEffect, useRef, useCallback } from 'react'
import { ReactSVG } from 'react-svg'
import { useHistory } from 'react-router-dom'
import ChatHttpServer  from '../../../utils/chatHttpServer'
import ChatSocketServer  from '../../../utils/chatSocketServer'
import ChatList from '../../components/chat-list'
import MessageBox from '../../components/message-box'
import './home.scss'

const HomePage = () => {
	let userId = useRef(null)
	const [selectedUser, setSelectedUser] = useState(null)
	const [username, setUsername] = useState('')
	const [isLoading, setIsLoading] = useState(true)
	const [isMenuOpen, setIsMenuOpen] = useState(false)
	const history = useHistory()

	const logout = async () => {
		try {
			await ChatHttpServer.removeLS();
			ChatSocketServer.logout({ userId: userId.current })
			ChatSocketServer.eventEmitter.on('logout-response', (loggedOut) => {
				history.push(`/`);
			})
		} catch (error) {
		  	console.log(error)
		  	console.log(' This App is Broken, we are working on it. try after some time.')
		}
	}

	const loadUser = useCallback(async () => {
		try {
			setIsLoading(true);
			userId.current = await ChatHttpServer.getUserId();
		 	const response = await ChatHttpServer.userSessionCheck(userId.current);
		  	if (response.error) {
				history.push(`/`)
		  	} else {
				setUsername(response.username);
				ChatHttpServer.setLS('username', response.username);
				ChatSocketServer.establishSocketConnection(userId.current);
		  	}
		  	setIsLoading(false);
		} catch (error) {
			setIsLoading(false);
		  	history.push(`/`)
		}
	}, [history])

	useEffect(() => {
		(async () => {
			await loadUser();
		})()
	}, [loadUser])

	return (
		<div className='home-page'>
			<header className='header'>
				<nav>
					<div className="left-side">
						<div className="title">Socially Chat</div>
					</div>
					
					<div className="right-side">
						<div className="icons">
							<div className="notification-icon">
								<ReactSVG className="notification" src={'notification.svg'} />
								<span className='dot'></span>
							</div>
						</div>
						<div className="user-info">
							<div
								className="user-info-container"
								onMouseEnter={() => setIsMenuOpen(true)}
								onMouseLeave={() => setIsMenuOpen(false)}
							>
								<img
									style={{ backgroundColor: '#fd79a8' }}
									src={'male-user.png'} alt="profile"
								/>
								{
									userId.current && isMenuOpen &&
										<ul className="dropdown-menu">
											<li>{username.split(' ')[1]}</li>
											<li>Settings</li>
											<li onClick={logout}>Logout</li>
										</ul>
								}
							</div>
							{
								username && <p>{username}</p>
							}
							
						</div>
					</div>
				</nav>
			</header>

			<main className="main">
				<div className="sidebar">
					<div className="search">
						<input className='search-input' placeholder='Search' type="search"/>
						<ReactSVG className="search-icon" src={'search-30.svg'} />
					</div>
					{
						!isLoading && userId.current &&
						<ChatList
							userId={userId.current}
							updateSelectedUser={setSelectedUser}
						/>
					}
				</div>
				{
					selectedUser ?
						<MessageBox
							newSelectedUser={selectedUser}
							userId={userId.current}
						/> 
						:
						<div
							className="message-box"
							style={{
								display: 'flex',
								justifyContent: 'center',
								alignItems: 'center',
								fontSize: '30px'
							}}
						>
							<h2>Choose a user from the chatlist.</h2>
						</div>
				}
				
			</main>
		</div>
	)
}

export default HomePage
